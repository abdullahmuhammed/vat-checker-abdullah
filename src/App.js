import React from "react";
import VatInput from "./components/VatInput";
import Navbar from "./components/Navbar";
import MainContent from "./components/MainContent";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Contact from "./Contact";

function Home() {
  return (
    <div className="App container">
      <Navbar page={"home"} />
      <MainContent />
      <VatInput />
    </div>
  );
}

function App() {
  return (
    <Router>
      <div className="App container">
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/contact" component={Contact} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
