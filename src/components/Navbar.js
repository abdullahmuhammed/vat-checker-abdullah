import React from "react";
import { Link } from "react-router-dom";

function Navbar(props) {
  let pageType;
  if (props.page === "home") {
    pageType = "VAT Checker";
  } else if (props.page === "contact") {
    pageType = "Contact";
  }
  return (
    <div>
      <nav className="navbar navbar-expand-lg m-4 navbar-light bg-transparent">
        <h1 className="navbar-brand text-white">{pageType}</h1>

        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav ml-auto">
            <li className="nav-item active">
              <Link to="/" className="nav-link text-white">
                Home
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/contact" className="nav-link text-white">
                Contact
              </Link>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  );
}

export default Navbar;
