import React from "react";

function ResponseDiv(props) {
  if (props.vatObject.valid) {
    return (
      <div className="response-valid text-center">
        <h6 className="p-2 text-white">
          The number is valid on the specified date.
        </h6>
        <p className="text-white font-weight-bold p-2 text-left">
          Trader Name: {props.vatObject.company_name}
          <span />
        </p>
        <p className="text-white font-weight-bold p-2 text-left pb-3">
          Trader Address: {props.vatObject.company_address}
          <span />
        </p>
      </div>
    );
  } else if (!props.vatObject.valid && props.isLoaded === true) {
    return (
      <div className="response-invalid text-center">
        <h6 className="p-2 text-white">
          The number is invalid on the specified date.
        </h6>
      </div>
    );
  } else {
    return <div />;
  }
}

export default ResponseDiv;
