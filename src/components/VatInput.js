import React, { Component } from "react";
import ResponseDiv from "./ResponseDiv";
import axios from "axios";

class VatInput extends Component {
  state = {
    number: "",
    vatObject: {},
    isLoaded: false
  };

  handleChange = event => {
    this.setState({ number: event.target.value });
  };

  handleSubmit = event => {
    event.preventDefault();
    const vatNumber = {
      number: this.state.number
    };
    console.log(vatNumber);
    axios
      .get(
        `http://www.apilayer.net/api/validate?access_key=6aec7d82df3be01b3a7fae6aa01d7b4a&vat_number=${
          vatNumber.number
        }`
      )
      .then(response => {
        this.setState({
          vatObject: response.data,
          isLoaded: true
        });
        console.log(response.data);
      })
      .catch(error => {
        console.log(error);
      });
  };
  render() {
    return (
      <div className="m-auto text-center pt-3">
        <h1 className="text-white py-3">VAT Checker</h1>
        <form onSubmit={this.handleSubmit} className="form">
          <input
            type="text"
            name="vatNumber"
            className="form__field"
            placeholder="LU26375245"
            onChange={this.handleChange}
          />
          <button
            type="submit"
            className="btn btn--primary btn--inside uppercase"
          >
            {" "}
            Check{" "}
          </button>
        </form>
        <ResponseDiv
          vatObject={this.state.vatObject}
          isLoaded={this.state.isLoaded}
          number={this.state.number}
        />
      </div>
    );
  }
}

export default VatInput;
