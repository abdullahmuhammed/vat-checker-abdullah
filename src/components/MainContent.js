import React from "react";

function MainComponent() {
  return (
    <div className="row m-4">
      <div className="col-5 main-content">
        <div className="mt-5">
          <h1 className="text-white mt-5">
            Check the <br />
            Value Added Tax Number
          </h1>
          <p className="mt-4 mr-5 vat-par">
            VAT Checker allows you to check the validity of a VAT number prior
            to applying the 0% rate when selling goods or services to EU
            countries.{" "}
          </p>
        </div>
      </div>
      <div className="col-7 pl-5 main-content">
        {" "}
        <img
          className="img-fluid building-img"
          alt="Business building"
          src="http://pngimg.com/uploads/building/building_PNG87.png"
        />{" "}
      </div>
    </div>
  );
}

export default MainComponent;
